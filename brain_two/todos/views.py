from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, TodoItemForm


# Create your views here.
def todo_list_list(request):
    todolist = TodoList.objects.all()
    count = TodoList.objects.count()
    context = {
        "todo_list_list": todolist,
        "count": count,
    }
    return render(request, "lists.html", context)


def todo_list_detail(request, id):
    detail = get_object_or_404(TodoList, id=id)

    context = {
        "list_detail": detail,
    }
    return render(request, "details.html/", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            model_instance = form.save()
            return redirect("list_detail", id=model_instance.id)
    else:
        form = TodoForm()

    context = {
        "form": form,
        }

    return render(request, "create.html/", context)

def todo_list_update(request, id):
    detail = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=detail)
        if form.is_valid():
            model_instance = form.save()
            return redirect("list_detail", id=model_instance.id)
    else:
        form = TodoForm(instance=detail)

    context = {
        "form": form,
    }

    return render(request, "edit.html/", context)

def todo_list_delete(request, id):
    detail = TodoList.objects.get(id=id)
    if request.method == "POST":
        detail.delete()
        return redirect("show_list")

    return render(request, "delete.html/")

def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            model_instance = form.save()
            return redirect("list_detail", id=model_instance.list.id)
    else:
        form = TodoItemForm()

    context = {
        "form": form,
        }

    return render(request, "create_item.html/", context)

def todo_item_update(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            model_instance = form.save()
            return redirect("list_detail", id=model_instance.list.id)
    else:
        form = TodoItemForm(instance=item)

    context = {
        "form": form,
    }

    return render(request, "item_edit.html/", context)
